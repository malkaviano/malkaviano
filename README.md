- 👋 Hi, I’m @malkaviano
- 👀 I’m interested in Games, Scala, Chess, pushing my knowledge to the limit and making next generation software.
- 🌱 I’m currently learning Scala, Akka and new stuff.
- 💞️ I’m looking to collaborate on Video Game community.
- 📫 How to reach me: github.

<!---
malkaviano/malkaviano is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
